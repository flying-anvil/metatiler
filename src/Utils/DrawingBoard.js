// @flow

class DrawingBoard {
  canvas;
  context;
  scale;
  tileSize;

  constructor (canvas, scale, tileSize) {
    this.scale = scale;
    this.tileSize = tileSize;
    this.canvas = canvas;
    this.context = canvas.getContext('2d');
  }
}

export default DrawingBoard;
