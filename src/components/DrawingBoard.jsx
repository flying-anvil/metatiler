import React, {useState} from 'react';
import Canvas from "./Canvas";

type
Props = {};

const DrawingBoard = () => {
  const [color, setColor] = useState('#4EA3BA');
  console.log(color);

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-8">
          <Canvas
            scale={8}
            tileSize={8}
            color={color}
          />
        </div>
        <div className="col-4">
          <div style={{backgroundColor: '#0ff'}} onClick={() => setColor(`#${Math.floor(Math.random() * 1000000)}`)}>tse</div>
        </div>
      </div>
    </div>
  );
};

export default DrawingBoard;
