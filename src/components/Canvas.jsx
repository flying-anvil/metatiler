import {useEffect} from "react";
import './DrawinBoard.scss';

const {useRef} = require("react");

const getMousePos = (canvas, event) => {
  const rect = canvas.getBoundingClientRect(); // abs. size of element
  const scaleX = canvas.width / rect.width;    // relationship bitmap vs. element for X
  const scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

  return {
    x: (event.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
    y: (event.clientY - rect.top) * scaleY     // been adjusted to be relative to element
  }
}

const Canvas = (props) => {
  const scale    = props.scale || 8;
  const tileSize = props.tileSize || 8;
  const color    = props.color || '#333'

  let pos    = {x: 0, y: 0};
  let doDraw = false;

  const updatePosition = (event) => {
    pos = getMousePos(event.target, event)

    if (doDraw) {
      draw(contextRef.current);
    }
  }

  const onDown = (event) => {
    if (event.button !== 0) return;

    doDraw = true;

    draw(contextRef.current)
  }

  const onUp = (event) => {
    if (event.button !== 0) return;

    doDraw = false;
  }

  const draw = (context) => {
    if (!doDraw) return;

    context.fillStyle = color;
    const x = Math.floor(pos.x / scale) * scale;
    const y = Math.floor(pos.y / scale) * scale;
    context.fillRect(x, y, scale, scale);
    drawGrid(context);
  }

  const drawCheckers = (context) => {
    for (let row = 0; row < (16 * tileSize); row++) {
      for (let col = 0; col < 128; col++) {
        const x = col * scale;
        const y = row * scale;

        context.fillStyle = (row + col) % 2 === 0
          ? '#727272'
          : '#878787';
        context.fillRect(x, y, scale, scale)

        // context.fillStyle = '#85724E';
        // context.fillRect(x, y, scale, scale);
        //
      }
    }
  }

  const drawGrid = (context) => {
    for (let tileRow = 0; tileRow < (16 * tileSize) / tileSize; tileRow++) {
      for (let tileCol = 0; tileCol < 128 / tileSize; tileCol++) {
        const x = tileCol * scale * 8;
        const y = tileRow * scale * 8;

        context.strokeStyle = '#666';
        context.strokeRect(x + .5, y + .5, (scale * tileSize) - 1, (scale * tileSize) - 1);
      }
    }
  }

  const drawImage = (context, imageUrl) => {
    const image = new Image(16, 16);
    image.src = imageUrl;
    image.onload = () => {
      console.log({image});
      context.drawImage(image, 0, 0, tileSize * scale, tileSize * scale);
      drawGrid(context);
    }
  }

  const canvasRef  = useRef();
  const contextRef = useRef();
  useEffect(() => {
    const canvas  = canvasRef.current;
    const context = canvas.getContext('2d');

    context.fillStyle = '#aaa';
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);

    drawCheckers(context);
    drawGrid(context);

    // drawImage(context,  '/favicon.ico');

    contextRef.current = context;
  }, [canvasRef])

  return (
    <div className="canvas">
      <canvas
        className="pixelated drawable"
        width={16 * tileSize * scale}
        height={128 * scale}
        ref={canvasRef}
        onMouseDown={onDown}
        onMouseUp={onUp}
        onMouseMove={updatePosition}
      />
    </div>
  );
};

export default Canvas;
